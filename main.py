# ____                        _           _    ____ ___
#/ ___|  __ _ _ __ ___  _ __ | | ___     / \  |  _ \_ _|
#\___ \ / _` | '_ ` _ \| '_ \| |/ _ \   / _ \ | |_) | |   _____
# ___) | (_| | | | | | | |_) | |  __/  / ___ \|  __/| |  |_____|
#|____/ \__,_|_| |_| |_| .__/|_|\___| /_/   \_\_|  |___|
#                      |_|

import pprint
import datetime
import pandas as pd
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from insyloapi import InsyloAPI

register_matplotlib_converters()

username = "USER"
password = "PASSWORD"
    
if __name__ == "__main__":    
    # Connect to the API and retrieve a new token 
    insylo = InsyloAPI(username, password)
    
    # Set a data time window to work with
    end_date_d = datetime.datetime(2020, 1, 27, 20, 00, 00)
    start_date_d = end_date_d + datetime.timedelta(days=-5)
    
    ##############################################    
    # _                                         _
    #| |    ___   ___  _ __     ___  _ __      / \   _ __ ___  __ _ ___
    #| |   / _ \ / _ \| '_ \   / _ \| '_ \    / _ \ | '__/ _ \/ _` / __|
    #| |__| (_) | (_) | |_) | | (_) | | | |  / ___ \| | |  __/ (_| \__ \
    #|_____\___/ \___/| .__/   \___/|_| |_| /_/   \_\_|  \___|\__,_|___/
    #                 |_|

    # We could apply to a group of bins by looping on areas
    areas = insylo.get_areas()
    binUUID = ""
    # Get bins from each area
    for area in areas["data"]:
        bins = insylo.get_bins(area["id"])
        try:
            # Here we just print some information from area -> bin relationship.
            for bi in bins["data"]:
                print("{} :: {} :: {} :: {}".format(bi['uuid'], area['label'],area['description'], bi['label']))

                # Work with an specific BIN                
                binUUID = bi['uuid']
                # Get reading from a given bin and a time window
                readings = insylo.get_metrics(binUUID, start_date_d, end_date_d)
                # Get all readings from the previous N days
                weights = [[sample['id'], 
                            sample['timestamp']['createdAt'],
                            float(sample['weight'])] for sample in readings['data']]
                
                #Create DataFrame to plot timeserie
                data = pd.DataFrame(weights, columns=['id', 'ts', 'weight'])
                data['ts'] = pd.to_datetime(data['ts'])
                data.set_index('ts')
            
                ##############################################
                # Detect refillings greated that maxLoad tonnes
                # assuming hourly readings, direct substraction between 
                # current and previous data, give us a material variation.  
                maxLoad = 1.0                    
                # Every variation larger than maxLoad will be accepted as a refilling
                data['lag_1'] = data.weight.shift(-1)
                data['lag_1'] = data.lag_1.fillna(method='backfill')
                data['diff']  = data.weight - data.lag_1
                isLoad = data['diff']>maxLoad
                data['stock']  = data[isLoad]['weight']
                data.sort_index(ascending=False, inplace=True)
                        
                ##############################################
                # Plot time series to validate refilling detection
                fig = plt.figure(constrained_layout=True,figsize=(8,6), dpi=100)
                ax = fig.add_subplot(111)
                ax.plot(data['ts'], data['weight'] , 'k', alpha=0.5)
                ax.plot(data['ts'], data['stock'] , 'r+', alpha=1)
                ax.set_xlabel('Date')
                ax.set_ylabel('Bin stock (Tn)')
                #set major ticks format
                ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))   
                plt.show()
                plt.close()


        except:
            print('No bins')
                            
            
